@extends('app')

@section('content')
	<div>{!! $complaints->render() !!}</div>
	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th>Product</th>
			<th>Sub Product</th>
			<th>Issue</th>
			<th>Received</th>
			<th>Sent to Company</th>
			<th>Company</th>
			<th>Company Response</th>
			<th>Timely?</th>
			<th>Disputed?</th>
		</tr>
		</thead>
		<tbody>
		@foreach($complaints as $complaint)
			<tr>
				<td>{{ $complaint->product }}</td>
				<td>{{ $complaint->subproduct }}</td>
				<td>{{ $complaint->issue }}</td>
				<td>{{ $complaint->datereceived->format('m/d/Y') }}, via {{ $complaint->submittedvia }}</td>
				<td>{{ $complaint->datesenttocompany->format('m/d/Y') }}</td>
				<td>{{ $complaint->company }}, {{ $complaint->state }} {{ $complaint->zipcode }}</td>
				<td>{{ $complaint->companyresponse }}</td>
				<td>{{ $complaint->timelyresponse }}</td>
				<td>{{ $complaint->consumerdisputed }}</td>

			</tr>
		@endforeach
		</tbody>
	</table>
<div>{!! $complaints->render() !!}</div>
@endsection
