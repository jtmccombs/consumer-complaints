## Basic Install

### Info needed

1. $mysqlUsername
2. $mysqlPassword
3. $databaseName
4. MySQL File


### Database Setup
SSH Into Server

    ssh user@server.com
    
Create a database in MySQL
    
    mysql -u $mysqlUsername -p'$mysqlPassword'
    CREATE DATABASE $databaseName;
    exit;

Import SQL file

    mysql -u $mysqlUsername -p'$mysqlPassword' $databaseName < file.sql

Install prerequisites
    
    # Install Git
    sudo apt-get install git
    
    # Enable mcrypt
    sudo php5enmod mcrypt
    
    # Restart Apache
    sudo apachectl restart

Clone Repo

    cd /var/www
    git clone https://jtmccombs@bitbucket.org/jtmccombs/consumer-complaints.git
    sudo chown -R www-data:www-data consumer-complaints
    
Set up Apache Host

    cd /etc/apache2/sites-enabled
    sudo nano ./default
    
    // Change the server folder to /var/www/consumer-complaints/public
    
    # Restart Apache
    sudo apachectl restart

Install Composer

    cd ~/
    curl -sS https://getcomposer.org/installer | php
    sudo mv composer.phar /usr/local/bin/composer
    
Set up Laravel Environment Variables

    cd /var/www/consumer-complaints
    cp .env.example .env
    nano .env
    // Fill the database information into the appropriate fields, save, and exit
    
Set up Laravel

    cd /var/www/consumer-complaints
    sudo composer install
    sudo chown -R www-data:www-data /var/www/consumer-complaints


    


 
 
  