<?php namespace ConsumerComplaints;
/**
 * Created by Justin McCombs.
 * jtmccombs@gmail.com
 * Date: 6/5/15
 * Time: 3:01 PM
 */

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Complaint extends \Eloquent {

    protected $guarded = ['id'];

    public static $rules = [

    ];
    
    /*
	|--------------------------------------------------------------------------
	| Rules
	|--------------------------------------------------------------------------
	*/
	public static function rules($id = null, $merge=[])
	{
		return array_merge(self::$rules, $merge);
	}
    

    /*
	|--------------------------------------------------------------------------
	| Relationships
	|--------------------------------------------------------------------------
	*/


    /*
	|--------------------------------------------------------------------------
	| Getters and Setters
	|--------------------------------------------------------------------------
	*/
	public function getDatereceivedAttribute($value)
	{
		return Carbon::parse($value);
	}

	public function getDatesenttocompanyAttribute($value)
	{
		return Carbon::parse($value);
	}

    /*
	|--------------------------------------------------------------------------
	| Repo Methods
	|--------------------------------------------------------------------------
    | These methods may be moved to a repository later on in the development
    | cycle as needed.
	*/

}